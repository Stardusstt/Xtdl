import time
import json
import requests


class Version_data:
    
    def get_json_dict( self , url ):
        
        response = requests.get( url )

        # json to dictionary
        json_dict = json.loads( response.text )

        return json_dict

    def get_template( self ):
        
        # Add edit time
        edit_time = time.ctime()
        template_dict = { 'edit_time': edit_time }
        

        # Add Component
        component = { 'component':{  } }
        template_dict.update( component )
        

        return template_dict

    def get_Xtdl( self ):

        # Add field
        Xtdl_dict = { 'Xtdl':{ 'latest_version': "" , 'download_url': "" }  }
        

        # Get data
        url = "https://api.github.com/repos/Stardusstt/Xtdl/releases/latest"
        json_dict_temp = self.get_json_dict( url )


        # Extract data
        version_data = json_dict_temp['tag_name']
        url_data = json_dict_temp['assets'][0]['browser_download_url']


        # Assignment
        Xtdl_dict['Xtdl']['latest_version'] = version_data
        Xtdl_dict['Xtdl']['download_url'] = url_data
    

        return Xtdl_dict
        
    def get_yt_dlp( self ):
                
        # Add field
        youtube_dl_dict = { 'yt-dlp':{ 'latest_version': "" , 'download_url': "" }  }
        

        # Get data
        url = "https://api.github.com/repos/yt-dlp/yt-dlp/releases/latest"
        json_dict_temp = self.get_json_dict( url )


        # Extract data
        version_data = json_dict_temp['tag_name']

        # extract download_url
        for temp_dict in json_dict_temp['assets'] :

            if temp_dict['name'] == "yt-dlp.exe" :

                url_data = temp_dict['browser_download_url']
        

        # Assignment
        youtube_dl_dict['yt-dlp']['latest_version'] = version_data
        youtube_dl_dict['yt-dlp']['download_url'] = url_data
    

        return youtube_dl_dict
    
    def get_FFmpeg( self ):
                
        # Add field
        FFmpeg_dict = { 'FFmpeg':{ 'latest_version': "" , 'download_url': "" , 'tools_download_url': { 'ffmpeg': "" , 'ffprobe': "" } }  }
        

        # Get - latest build success sha (version_data)
        url = "https://api.github.com/repos/BtbN/FFmpeg-Builds/tags"
        json_dict_temp = self.get_json_dict( url )

        # extract
        version_data = json_dict_temp[0]['commit']['sha']


        # Get - browser_download_url
        url = "https://api.github.com/repos/BtbN/FFmpeg-Builds/releases/latest"
        json_dict_temp = self.get_json_dict( url )

        # extract
        for temp_dict in json_dict_temp['assets'] :

            if temp_dict['name'] == "ffmpeg-master-latest-win64-gpl.zip" :

                url_data = temp_dict['browser_download_url']


        # Assignment
        FFmpeg_dict['FFmpeg']['latest_version'] = version_data
        FFmpeg_dict['FFmpeg']['download_url'] = url_data

        # tools_url
        tools_url_data_ffmpeg = "https://github.com/Stardusstt/Xtdl/raw/master/.update/component_data/ffmpeg.exe"
        tools_url_data_ffprobe = "https://github.com/Stardusstt/Xtdl/raw/master/.update/component_data/ffprobe.exe"

        FFmpeg_dict['FFmpeg']['tools_download_url']['ffmpeg'] = tools_url_data_ffmpeg
        FFmpeg_dict['FFmpeg']['tools_download_url']['ffprobe'] = tools_url_data_ffprobe


        return FFmpeg_dict

    def create_json( self , filename ):

        # Create filename.json
        with open( filename+'.json' , 'w' , encoding='utf-8' ) as output:
            
            # Create - version dictionary
            output_temp = self.get_template()
            output_temp['component'].update( self.get_Xtdl() )
            output_temp['component'].update( self.get_yt_dlp() )
            output_temp['component'].update( self.get_FFmpeg() )
            

            # Dictionary to json 
            output_temp_json = json.dumps( output_temp , indent=2 )
            

            # Write to file
            output.write( output_temp_json )


def main():

    version_data = Version_data()


    version_data.create_json( 'version' )


if __name__ == "__main__":

    main()