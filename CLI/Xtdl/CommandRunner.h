#pragma once
#define NOMINMAX   // https://github.com/boostorg/regex/issues/130

#include <iostream>
#include <string>

#include "targetver.h"
#include <boost/regex.hpp>
#include <boost/process.hpp>


class CommandRunner
{
public:

	void ExecuteCommand( std::string command );

	
	std::string state() const;
	std::string speed() const;
	std::string eta() const;

private:

	void ParseDownload( std::string const& output );


	std::string state_ , speed_ , eta_ ;

};

