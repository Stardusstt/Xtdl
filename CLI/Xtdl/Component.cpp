#include "Component.h"


// Xtdl

std::string Xtdl::Version()
{
	return version_ ;
}

std::string Xtdl::CheckUpdate()
{
	

	return std::string();


}

void Xtdl::Update()
{
}

std::string Xtdl::update_output() const
{
	return update_output_ ;
}

// Youtube_dl

std::string Youtube_dl::Version()
{

	using namespace std ;
	using namespace boost::process ;


	ipstream temp ;
	string cli_string , version ;
	

	// Start a process
	child youtube_dl( "youtube-dl --version" , std_out > temp );

	while ( youtube_dl.running() ) // get word from temp to cli_string if child is running
	{
		getline( temp , cli_string );

		// concatenate to one string
		version.append( cli_string );
	}

	return version ;
	
}

std::string Youtube_dl::CheckUpdate()
{
	
	return std::string();


}

void Youtube_dl::Update()
{

	using namespace std ;
	using namespace boost::process ;


	ipstream temp ;
	string cli_string ;


	// Start a process
	child youtube_dl( "youtube-dl --update" , std_out > temp );

	while ( youtube_dl.running() ) // get word from temp to cli_string if child is running
	{
		getline( temp , cli_string );
		
		// prevent empty string 
		if ( cli_string.empty() != true )
		{
			update_output_ = cli_string ;
		}

	}

}

std::string Youtube_dl::update_output() const
{
	return update_output_ ;
}

// FFmpeg

std::string FFmpeg::Version()
{
	
	return std::string();

}

std::string FFmpeg::CheckUpdate()
{

	return std::string();



}

void FFmpeg::Update()
{
}

std::string FFmpeg::update_output() const
{
	return update_output_ ;
}
