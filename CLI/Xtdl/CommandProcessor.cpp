#include "CommandProcessor.h"


// CommandParameter

std::string CommandParameter::name() const
{
	return name_;
}

std::string CommandParameter::url() const
{
	return url_;
}

std::string CommandParameter::path() const
{
	return path_;
}

Media_format CommandParameter::media_format() const
{
	return media_format_;
}

Video_format CommandParameter::video_format() const
{
	return video_format_;
}

Audio_format CommandParameter::audio_format() const
{
	return audio_format_;
}

void CommandParameter::name( std::string name )
{
	name_ = std::move( name );
}

void CommandParameter::url( std::string url )
{
	url_ = std::move( url );
}

void CommandParameter::path( std::string path )
{
	path_ = std::move( path );
}

void CommandParameter::media_format( Media_format media_format )
{
	media_format_ = std::move( media_format );
}

void CommandParameter::video_format( Video_format video_format )
{
	video_format_ = std::move( video_format );
}

void CommandParameter::audio_format( Audio_format audio_format )
{
	audio_format_ = std::move( audio_format ) ;
}


// CommandProcessor
std::string CommandProcessor::CreateCommand( CommandParameter parameter )
{

	using namespace std ;


	auto CheckParameter = [&]() 
	{
	
		// Check if "name" is empty
		if ( parameter.name().empty() )
		{
			throw invalid_argument("Argument - name , is empty");
		}

		// Check if "url" is empty
		if ( parameter.url().empty() )
		{
			throw invalid_argument("Argument - url , is empty");
		}

		// Check if "path" is empty
		if ( parameter.path().empty() )
		{
			throw invalid_argument("Argument - path , is empty");
		}

		// Check if "media_format" is null
		if ( parameter.media_format() == Media_format::null )
		{
			throw invalid_argument("Argument - media_format , is null");
		}

		// Check if "video_format , audio_format" is null
		if ( parameter.video_format() == Video_format::null &&
			 parameter.audio_format() == Audio_format::null )
		{
			throw invalid_argument("Argument - video_format , audio_format , is null");
		}

	};
	CheckParameter();

	
	stringstream command_stream ;


	// Start Create
	command_stream << R"(youtube-dl  --newline)" ;


	// Location of the configuration file
	command_stream << R"(  --config-location ./youtube-dl.conf)" ;


	// Format Selection
	switch ( parameter.media_format() )
	{
	case Media_format::video_and_audio:
		// do 
		break;
	case Media_format::video_only:
		// do
		break;
	case Media_format::audio_only:
		command_stream << R"(  --format  bestaudio  --extract-audio)" ;

		switch ( parameter.audio_format() )
		{
		case Audio_format::original:
			// nothing
			break;
		case Audio_format::wav:
			command_stream << R"(  --audio-format wav)" ;
			break;
		}

		break;
	}
	
	
	// OUTPUT TEMPLATE

	//  --output "PATH/name.%(ext)s"
	command_stream << R"(  --output ")" << parameter.path() << R"(/)"
				   << parameter.name() << R"d(.%(ext)s")d" ;


	// URL
	command_stream  << " " << parameter.url() ;

	
	// Get the contents of underlying string object
	return command_stream.str();

}

