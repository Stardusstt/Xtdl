#pragma once

#include <iostream>
#include <string>
#include <sstream>
#include <stdexcept>


// Enumeration for Media_format
enum class Media_format
{
	null , video_and_audio , video_only , audio_only
};

// Enumeration for Video_format
enum class Video_format
{
	null 
};

// Enumeration for Audio_format
enum class Audio_format
{
	null , original , wav
};

class CommandParameter 
{
public:

	std::string name() const;
	std::string url() const;
	std::string path() const;
	Media_format media_format() const;
	Video_format video_format() const;
	Audio_format audio_format() const;

	void name( std::string name );
	void url( std::string url );
	void path( std::string path );
	void media_format( Media_format media_format );
	void video_format( Video_format video_format );
	void audio_format( Audio_format audio_format );
	
private:

	std::string name_ , url_ , path_ ;
	Media_format media_format_ { Media_format::null };
	Video_format video_format_ { Video_format::null };
	Audio_format audio_format_ { Audio_format::null };

};


class CommandProcessor
{
public:

	std::string CreateCommand( CommandParameter parameter );

private:

	
};

