﻿#include <iostream>
#include <string> 
#include <limits>
#include <iomanip>
#include <future>
#include <chrono>
#include <stdexcept>

#include "CommandProcessor.h"
#include "CommandRunner.h"
#include "Component.h"


// Enumeration for mode
enum class EMode 
{
	// start from 1
	SingleDownload = 1 , ReadTxt , Component
};

// Enumeration for component
enum class EComponent 
{
	// start from 1
	Xtdl = 1 , Youtube_dl , FFmpeg
};

void SingleDownload()
{

	using namespace std ;


	CommandParameter commandParameter ;
	Media_format eMedia_format {} ;
	Video_format eVideo_format {} ;
	Audio_format eAudio_format {} ;
	int media_format {} , video_format {} , audio_format {} ;
	string url , filename , path ;

	
	system( "cls" );

	// URL
	cout << endl << " URL: ";
	cin >> url ;

	// discards max() amount of characters until finds a delimiter
	// ( ignore() also discards the delimiter if it finds it )
	// prevent getline() skip input
	cin.ignore( std::numeric_limits<std::streamsize>::max() , '\n' );

	commandParameter.url( url );


	// Name
	cout << endl << " Name: ";
	getline( cin , filename );
	commandParameter.name( filename );


	// Path
	cout << endl << " Path: ";
	getline( cin , path );
	commandParameter.path( path );


	// Download Type
	cout << endl << " (1) video_and_audio "
		 << endl << " (2) video_only "
		 << endl << " (3) audio_only "
		 << endl << " Download Type : " ;
	cin >> media_format ;
	cin.ignore( std::numeric_limits<std::streamsize>::max() , '\n' );

	eMedia_format = static_cast<Media_format>( media_format ); // int to enum 
	commandParameter.media_format( eMedia_format );


	// Format
	switch ( eMedia_format )
	{
	case Media_format::video_and_audio:
		// do 
		break;
	case Media_format::video_only:
		// do
		break;
	case Media_format::audio_only:
		
		cout << endl << " (1) Original "
			 << endl << " (2) WAV "
			 << endl << " Format : " ;
		cin >> audio_format ;
		cin.ignore( std::numeric_limits<std::streamsize>::max() , '\n' );

		eAudio_format = static_cast<Audio_format>( audio_format ); // int to enum 
		commandParameter.audio_format( eAudio_format );

		break;
	}


	// Process Command
	CommandProcessor commandProcessor ;
	string command ;


	try
	{
		//cout << commandProcessor.CreateCommand( commandParameter ) ;
		command = commandProcessor.CreateCommand( commandParameter ) ;
	}
	catch ( const invalid_argument& e ) // Catch CheckParameter() for commandProcessor
	{
		cout << e.what() << endl ;
		throw ;
	}
	

	// Execute Command
	CommandRunner commandRunner ;
	std::future<void> future_commandRunner ;
	
	
	try
	{
		future_commandRunner = 
			async( launch::async , [&]() { commandRunner.ExecuteCommand( command ); } );
	}
	catch ( const system_error& e ) // Catch "Boost.Process-child" for commandRunner
	{
		cout << e.what() << endl ;
		throw ;
	}
	

	// CLI Output
	auto Output = [&]() 
	{
		
		std::future_status commandRunner_status;

		do {

			// Get task status value
			commandRunner_status = future_commandRunner.wait_for( 0ms );


			// Check if task is finished
			switch ( commandRunner_status )
			{
			case std::future_status::deferred:
				
				throw invalid_argument("async launch deferred");

				break;
			case std::future_status::timeout:
				
				cout << "Task still running" << endl;
				//cout << "                                                                     ";
				//cout << "\r"; // back to line begin
				cout << commandParameter.name() << setw( 15 )
					 << commandRunner.state() << setw( 15 )
					 << commandRunner.speed() << setw( 10 )
					 << commandRunner.eta() ;
				//cout << "\r";
				cout << endl ;

				break;
			case std::future_status::ready:
				
				cout << "Task finished" << endl;

				break;
			}

		} while ( commandRunner_status != std::future_status::ready ); 

		system( "pause" );

	};

	try
	{
		Output();
	}
	catch ( const invalid_argument& e ) // Catch "async launch deferred" for Output
	{
		cout << e.what() << endl ;
		throw ;
	}

}

void Component()
{

	using namespace std ;


	EComponent eComponent {} ;
	int component {};


	system( "cls" );

	// Component select
	cout << endl << " (1) Xtdl "
		 << endl << " (2) Youtube_dl "
		 << endl << " (3) FFmpeg "
		 << endl << " Component : " ;
	cin >> component ;
	
	eComponent = static_cast<EComponent>( component ); // int to enum 
	

	// Action for Xtdl
	auto Action_Xtdl = []() 
	{
	
		Xtdl xtdl ;
		int action {} ;


		// Action select
		cout << endl << " (1) Version "
			 << endl << " (2) CheckUpdate "
			 << endl << " (3) Update "
			 << endl << " Action : " ;
		cin >> action ;


		// Action
		switch ( action )
		{
		case 1: // Version
		{

			// Execute Action
			std::future<string> future_xtdl ;


			try
			{
				future_xtdl = 
					async( launch::async , [&]() { return xtdl.Version(); } );
			}
			catch ( const system_error& e ) // Catch "Boost.Process-child" for xtdl
			{
				cout << e.what() << endl ;
				throw ;
			}


			// CLI Output
			auto Output = [&]() 
			{

				std::future_status commandRunner_status;

				do {

					// Get task status value
					commandRunner_status = future_xtdl.wait_for( 0ms );

					// Check if task is finished
					switch ( commandRunner_status )
					{
					case std::future_status::deferred:

						throw invalid_argument("async launch deferred");

						break;
					case std::future_status::timeout:

						cout << "Task still running" << endl;
						
						break;
					case std::future_status::ready:

						cout << "Task finished" << endl;
						cout << future_xtdl.get() << endl;

						break;
					}

				} while ( commandRunner_status != std::future_status::ready ); 

				system( "pause" );

			};

			try
			{
				Output();
			}
			catch ( const invalid_argument& e ) // Catch "async launch deferred" for Output
			{
				cout << e.what() << endl ;
				throw ;
			}


			break ;
		}
		case 2: // CheckUpdate
		{


			break ;
		}
		case 3: // Update
		{


			break ;
		}
		default:

			cout << "Exit";
			system( "pause" );

			break ;
		}

	};


	// Action for Youtube_dl
	auto Action_Youtube_dl = []() 
	{

		Youtube_dl youtube_dl ;
		int action {} ;


		// Action select
		cout << endl << " (1) Version "
			<< endl << " (2) CheckUpdate "
			<< endl << " (3) Update "
			<< endl << " Action : " ;
		cin >> action ;


		// Action
		switch ( action )
		{
		case 1: // Version
		{

			// Execute
			std::future<string> future_youtube_dl ;


			try
			{
				future_youtube_dl = 
					async( launch::async , [&]() { return youtube_dl.Version(); } );
			}
			catch ( const system_error& e ) // Catch "Boost.Process-child" for youtube_dl
			{
				cout << e.what() << endl ;
				throw ;
			}


			// CLI Output
			auto Output = [&]() 
			{

				std::future_status commandRunner_status;

				do {

					// Get task status value
					commandRunner_status = future_youtube_dl.wait_for( 0ms );


					// Check if task is finished
					switch ( commandRunner_status )
					{
					case std::future_status::deferred:

						throw invalid_argument("async launch deferred");

						break;
					case std::future_status::timeout:

						cout << "Task still running" << endl;

						break;
					case std::future_status::ready:

						cout << "Task finished" << endl;
						cout << future_youtube_dl.get() << endl;

						break;
					}

				} while ( commandRunner_status != std::future_status::ready ); 

				system( "pause" );

			};

			try
			{
				Output();
			}
			catch ( const invalid_argument& e ) // Catch "async launch deferred" for Output
			{
				cout << e.what() << endl ;
				throw ;
			}

			break ;
		}
		case 2: // CheckUpdate
		{


			break ;
		}
		case 3: // Update
		{

			// Execute
			std::future<void> future_youtube_dl ;


			try
			{
				future_youtube_dl = 
					async( launch::async , [&]() { youtube_dl.Update(); } );
			}
			catch ( const system_error& e ) // Catch "Boost.Process-child" for youtube_dl
			{
				cout << e.what() << endl ;
				throw ;
			}


			// CLI Output
			auto Output = [&]() 
			{

				std::future_status commandRunner_status;

				do {

					// Get task status value
					commandRunner_status = future_youtube_dl.wait_for( 0ms );


					// Check if task is finished
					switch ( commandRunner_status )
					{
					case std::future_status::deferred:

						throw invalid_argument("async launch deferred");

						break;
					case std::future_status::timeout:

						cout << "Task still running" << endl;
						cout << youtube_dl.update_output() << endl ;

						break;
					case std::future_status::ready:

						cout << "Task finished" << endl;

						break;
					}

				} while ( commandRunner_status != std::future_status::ready ); 

				system( "pause" );

			};

			try
			{
				Output();
			}
			catch ( const invalid_argument& e ) // Catch "async launch deferred" for Output
			{
				cout << e.what() << endl ;
				throw ;
			}

			break ;
		}
		default:

			cout << "Exit";
			system( "pause" );

			break ;
		}

	};


	// Action for FFmpeg
	auto Action_FFmpeg = []() 
	{

		FFmpeg fFmpeg ;
		int action {} ;


		// Action select
		cout << endl << " (1) Version "
			<< endl << " (2) CheckUpdate "
			<< endl << " (3) Update "
			<< endl << " Action : " ;
		cin >> action ;


		// Action
		switch ( action )
		{
		case 1: // Version
		{

			// Execute Action
			std::future<string> future_fFmpeg ;


			try
			{
				future_fFmpeg = 
					async( launch::async , [&]() { return fFmpeg.Version(); } );
			}
			catch ( const system_error& e ) // Catch "Boost.Process-child" for xtdl
			{
				cout << e.what() << endl ;
				throw ;
			}


			// CLI Output
			auto Output = [&]() 
			{

				std::future_status commandRunner_status;

				do {

					// Get task status value
					commandRunner_status = future_fFmpeg.wait_for( 0ms );

					// Check if task is finished
					switch ( commandRunner_status )
					{
					case std::future_status::deferred:

						throw invalid_argument("async launch deferred");

						break;
					case std::future_status::timeout:

						cout << "Task still running" << endl;

						break;
					case std::future_status::ready:

						cout << "Task finished" << endl;
						cout << future_fFmpeg.get() << endl;

						break;
					}

				} while ( commandRunner_status != std::future_status::ready ); 

				system( "pause" );

			};

			try
			{
				Output();
			}
			catch ( const invalid_argument& e ) // Catch "async launch deferred" for Output
			{
				cout << e.what() << endl ;
				throw ;
			}


			break ;
		}
		case 2: // CheckUpdate
		{


			break ;
		}
		case 3: // Update
		{


			break ;
		}
		default:

			cout << "Exit";
			system( "pause" );

			break ;
		}

	};


	// Execute Action
	switch ( eComponent )
	{
	case EComponent::Xtdl:

		Action_Xtdl();

		break ;
	case EComponent::Youtube_dl:
		
		Action_Youtube_dl();

		break ;
	case EComponent::FFmpeg:
		
		Action_FFmpeg();

		break ;
	default:
		
		cout << "Exit";
		system( "pause" );
		
		break ;
	}

}

int main()
{

	using namespace std ;


	EMode eMode;


	auto ChooseMode = []( EMode eMode )
	{

		switch ( eMode )
		{
		case EMode::SingleDownload:

			SingleDownload();

			break ;
		case EMode::ReadTxt:
			
			cout << "ReadTxt";
			system( "pause" );

			break ;
		case EMode::Component:

			Component();
			
			break ;
		default:
			
			cout << "default";
			system( "pause" );

			break ;
		}

	};
	int mode_choose {} ;


	while ( 1 )
	{

		system( "cls" );

		
		cout << endl << " (1) Single download "
			 << endl << " (2) Reading from txt "
			 << endl << " (3) Component "
			 << endl
			 << endl << " Choose the number : " ;
		cin >> mode_choose;

		eMode = static_cast<EMode>( mode_choose ); // int to enum 

		ChooseMode( eMode );

	}

}





