#include "CommandRunner.h"


void CommandRunner::ParseDownload( std::string const &output )
{

	using namespace boost ;


	// result for string objects
	smatch result; 

	//
	// [youtube] 7kmY6QOFMGg: Downloading webpage
	// 
	regex regex_youtube( "youtube" ); // match "youtube"
	if ( regex_search( output , regex_youtube ) )
	{

		regex regex_youtube_state( ":(.*)" ); // match " Downloading webpage" after ":"
		if ( regex_search( output , result , regex_youtube_state ) )
		{
			state_ = result[1] ; // Group 1.
		}

		return ;

	}

	//
	// [download] Destination: Location\filename.m4a
	// [download]  16.5% of 6.05MiB at  1.33MiB/s ETA 00:03
	// [download] 100% of 6.05MiB in 00:02
	//
	regex regex_download( "download" ); // match "download"
	if ( regex_search( output , regex_download ) )
	{

		regex regex_state( R"(\d.*%)" ); //  / \d.*% /  match "16.5%"
		if ( regex_search( output , result , regex_state ) )
		{
			state_ = result[0]; // Full match
		}

		regex regex_speed( "at +(.*s)" ); //  / at +(.*s) /  match "1.33MiB/s"
		if ( regex_search( output , result , regex_speed ) )
		{
			speed_ = result[1]; // Group 1.
		}

		regex regex_eta( R"d(ETA (.*\d))d" ); //  / ETA (.*\d) /  match "00:03"
		if ( regex_search( output , result , regex_eta ) )
		{
			eta_ = result[1]; // Group 1.
		}

		return ;

	}

	//
	// [ffmpeg] Destination: Location\filename.wav
	//
	regex regex_ffmpeg( "ffmpeg" ); // match "ffmpeg"
	if ( regex_search( output , regex_ffmpeg ) )
	{
		state_ = "Converting";
		speed_ = "-";
		eta_ = "-";

		return ;

	}

	//
	// Deleting original file Location\filename.m4a (pass -k to keep)
	//
	regex regex_deleting( "Deleting" ); // match "Deleting"
	if ( regex_search( output , regex_deleting ) )
	{
		state_ = "Cleaning Temp Files";
		speed_ = "-";
		eta_ = "-";

		return ;

	}

}

void CommandRunner::ExecuteCommand( std::string command )
{

	using namespace std ;
	using namespace boost::process ;


	ipstream temp ;
	string cli_string ;

	
	// Start a process
	child youtube_dl( command , std_out > temp );

	while ( youtube_dl.running() ) // get word from temp to cli_string if child is running
	{

		getline( temp , cli_string );
		ParseDownload( cli_string );

	}

}

std::string CommandRunner::state() const
{
	return state_ ;
}

std::string CommandRunner::speed() const
{
	return speed_ ;
}

std::string CommandRunner::eta() const
{
	return eta_ ;
}
