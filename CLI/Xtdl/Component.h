#pragma once

#include <iostream>
#include <string>

#include "targetver.h"
#include <boost/process.hpp>


class Xtdl
{
public:
	
	std::string Version();
	std::string CheckUpdate();
	void Update();

	std::string update_output() const;

private:
	
	std::string version_ { "0.6.0" } ; // Xtdl Version
	std::string update_output_ ;

};

class Youtube_dl
{
public:

	std::string Version();
	std::string CheckUpdate();
	void Update();

	std::string update_output() const;

private:

	std::string update_output_ ;

};

class FFmpeg
{
public:

	std::string Version();
	std::string CheckUpdate();
	void Update();

	std::string update_output() const;

private:

	std::string update_output_ ;

};